package com.example.myapplication.coffee;

import javax.inject.Inject;

public class Thermosiphon implements Pump {
    private final Heater heater;

    @Inject
    Thermosiphon(Heater heater) {
        this.heater = heater;
    }

    @Override
    public String pump() {
        if (heater.isHot()) {
            System.out.println("=> => pumping => =>");
            return "=> => pumping => =>\n";
        }

        return "\n";
    }
}
