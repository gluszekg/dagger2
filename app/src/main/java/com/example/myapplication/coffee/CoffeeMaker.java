package com.example.myapplication.coffee;

import dagger.Lazy;

import javax.inject.Inject;

public class CoffeeMaker {
    private final Lazy<Heater> heater; // Create a possibly costly heater only when we use it.
    private final Pump pump;

    @Inject
    CoffeeMaker(Lazy<Heater> heater, Pump pump) {
        this.heater = heater;
        this.pump = pump;
    }

    public String brew() {
        String on = heater.get().on();
        String pump = this.pump.pump();
        System.out.println(" [_]P coffee! [_]P ");
        String coffee = " [_]P coffee! [_]P \n";
        heater.get().off();

        return on + pump + coffee;
    }
}
