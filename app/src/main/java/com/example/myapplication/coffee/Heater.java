package com.example.myapplication.coffee;

public interface Heater {
    String on();

    void off();

    boolean isHot();
}
