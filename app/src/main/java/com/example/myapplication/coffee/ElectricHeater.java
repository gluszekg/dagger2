package com.example.myapplication.coffee;

public class ElectricHeater implements Heater {
    boolean heating;

    @Override
    public String on() {
        System.out.println("~ ~ ~ heating ~ ~ ~");
        this.heating = true;
        return "~ ~ ~ heating ~ ~ ~\n";
    }

    @Override
    public void off() {
        this.heating = false;
    }

    @Override
    public boolean isHot() {
        return heating;
    }
}
